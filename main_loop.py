#!/usr/bin/env python3
import time
import check_subs

def main():
	print("Bot powering up!")
	with open("settings.txt") as f:
		settings_dict  = eval(f.read())
	while True:
		try:
			check_subs.main(console=True)
			print("Bot entering standby for {loop_delay} minutes.".format(loop_delay=settings_dict['loop-delay']/60))
			time.sleep(settings_dict['loop-delay'])
		except KeyboardInterrupt:
			print("Exiting...")
			quit()
if __name__ == '__main__':
	main()