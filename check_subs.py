#!/usr/bin/env python3

import time
import praw
import pprint
import urllib.parse
import send_sms
# Plan:
# go to the links and download the gifs
def main(console=False):
	# load user settings
	with open("settings.txt") as f:
		settings = eval(f.read())
	with open("subs.txt") as f:
		subs = f.read().split('\n')
	with open("keywords.txt") as f:
		keywords = f.read().split('\n')
	with open("notified_links.txt") as f:
		old_ids = f.read().split('\n')
	# print(old_ids)
	# quit()
	# help(urllib.parse)
	user_agent = "Cyber Monday deal checker by Blazerboy65"
	r = praw.Reddit(user_agent=user_agent)

	ids = []
	links = []
	for sub in subs:
		subreddit = r.get_subreddit(sub)
		# begin scraping
		if console:
			print("Contacting ",sub)
		for post in subreddit.get_hot(limit=int(settings['max-posts'])):
			# get post links and ids for later reference
			try:
				for keyword in keywords:
					if keyword.lower() in post.title.lower() and not post.id in old_ids:
						print(post.title)
						ids.append(post.id)
						send_sms.send_text(post.title+'\n'+post.url)
						break
			except UnicodeEncodeError as e:
				print(e)
		# end scraping
		# save the post ids
	with open("notified_links.txt",'w') as f:
		f.write('\n'.join(old_ids + ids))

if __name__ == '__main__':
	main(console=True)